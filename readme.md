# IdeaSphere

This is where I place any ideas I have that:

1. I wish to implement myself, outside my professional career  
    This allows me to build open-source projects that additionally, may be of
    use to me at work. They will likely be built under the MIT license.
2. I am unable to build given my skillset, but wish to enter the world.  
    I am merely a software developer. More than likely I will not be able to
    vastly alter things outside of the software space.

I will link back my ideas when I implement them, or when other people create
them. Depending upon what type of idea it is.

If an idea I generate has already been implemented - feel free to submit a PR so
I can link it here!

