# Fuzzy Test

If the link is blue, I've made it! Boo!  
If it's purple, then I can't rhyme.

Testing software in an automated fashion can be hard. How do I know a set of
tests really covers the code well? Sure, there's definitely a discipline to it.  
However, I think the barrier for entry should be lowered...

In testing there is a "happy path", and a disastrous one. Ideally your code
should behave responsibly during both. In practice, however, there are often
small pockets of turbulence that need to be dealt with by the developer(s), and
found by the tester(s).

So, what if we were to cover not only the happy path, and the disaster path, but
also the areas betwixt? Well, that would mean you'd have to write substantially
more tests.

I propose an iterative test parser that fuzzily finds the pockets of turbulence
for you. You simply provide a few modules of happy, a few of disaster, and the
tests do the rest.

---

## Implementation musings

The approach will use a monad, a list of `Lanes :: [Action]`. When there is
only one lane, it will be as if there are none. The testing will be done by
mapping over the actions in a lane, and testing every combination possible
within that lane (except for none). This will be done for every state of every
lane. It's best to keep the lanes well organized and as minimal as possible.
Test runtime is an important factor. Additional rules can be added too. Though
the design of this framework should eliminate their need for most use cases...
One would do so by adding a test filter to see if certain tests should fail. As
there are assumptions made in code. In theory lanes should solve for this, but
only time will tell.

### Concept: Lane

A lane is simply an area (likely some fields on an object) that must be present.  
Your tests could be completely run through lanes, or there could be no lanes.  
The latter approach is good for fields that should handle empty/null/undefined
values.

### Concept: Action

A monadic term, simply modifies some state of the application under testing.
Will be either happy or disastrous, though the framework will not know which.
Must return 1+ log messages for test transparency.

### Other details

Monads greatly simplify this approach and allow it to generalize it to any
language. I will make multiple implementations of this project - C# (for
application at work, once it's ready), js/ts (very popular),
Java (I have a few projects there), Haskell (great language, probably simplest
implementation).  
For the tests to provide any insight, they should also log what they do.

